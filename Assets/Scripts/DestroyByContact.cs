﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;

    private GameController gameController;
    private PlayerController playerController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        else
        {
            Debug.Log("Cannot find 'GameController' script");
        }

        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if (playerControllerObject != null)
        {
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }
        else
        {
            Debug.Log("Cannot find 'PlayerController' script");
        }
    }

	void OnTriggerEnter(Collider other)
    {
        // Don't let the boundary destroy us
        if (other.tag == "Boundary")
        {
            return;
        }
        else if (gameObject.tag == "Hazard")
        {
            // Create explosions
            Instantiate(explosion, transform.position, transform.rotation);

            // Player logic
            if (other.tag == "Player")
            {
                Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
                gameController.GameOver();
            }
            else
            {
                // Update Score
                // Ramming is bad, don't reward bad behavior
                gameController.AddScore(scoreValue);
            }
        }
        else if (other.tag == "Powerup")
        {
            playerController.Powerup();
        }      

        // Cleanup
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
