﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour
{
    // Hazards
    public GameObject hazard;
    public Vector3 spawnValues;
    public int hazardCount;
    public float startWait;
    public float spawnWait;
    public float waveWait;

    // Display
    public GUIText scoreText;
    public GUIText restartText;
    public GUIText centerText;

    // Private
    private bool gameOver;
    private bool restart;
    private int score;

    void Start()
    {
        // Initialize
        score = 0;
        gameOver = false;
        restart = false;
        restartText.text = "";
        centerText.text = "";

        UpdateScore();
        StartCoroutine(SpawnWaves());
    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    IEnumerator SpawnWaves()
    {
        int currentHazardCount = hazardCount;
        int waveCount = 1;

        while (!gameOver)
        {
            centerText.text = "Wave " + waveCount;
            yield return new WaitForSeconds(startWait);
            centerText.text = "";

            for (int i = 0; i < currentHazardCount && !gameOver; ++i)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            waveCount += 1;
            hazardCount += 2;
            spawnWait *= 0.9f;
        }
       
        restartText.text = "Press 'R' to Restart";
        restart = true;
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    // Exposed
    public void AddScore(int scoreValue)
    {
        score += scoreValue;
        UpdateScore();
    }

    public void GameOver()
    {
        centerText.text = "Game Over!";
        gameOver = true;
    }
}
